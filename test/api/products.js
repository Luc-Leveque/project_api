'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/products');

const server = request(createServer());

describe.only('Product api', function() {
	before(async function() {
		await database.sequelize.query('DELETE from PRODUCTS');
		const {Products} = database;
		const promises = fixtures.map(products => Products.create(products));
		await Promise.all(promises);
});
 

		describe('POST /api/v1/products', function () {


		it("La requete envoie tous les données d'un product, product est crée et on reçoit un 200", async () => {
			await server.post('/api/v1/products')
				.send({
					name: 'orange',
					id_product: 10,
					price: 1
				})
				.expect(201);
		});

		it("La requete envoie un id_product qui existe déjà, on reçoit un 409", async () => {
			await server.post('/api/v1/products')
				.send({
					name: 'essaiprod2',
					id_product: 2,
					price: 400
				})
				.expect(409);
		});

        it("on reçoit une bad request", async () => {
            await server.post('/api/v1/products')
                .send({
                    name: 'banane',
					price: 3
                })
                .expect(400);
        });

    });

    describe('GET /api/v1/products', function() {

		it('Recover all users', async () => {
			const {body: Product} = await server
				.get('/api/v1/products')
				.set('Accept', 'application/json')
				.expect(200);

			expect(Product).to.be.an('array');
			expect(Product.length).to.equal(3);
			});
			

	});

	describe('GET /api/v1/products/:id_product', function() {

		it('L\'id donné existe donc j\'ai un 200 et un produit correspondant', async () => {
			const {body: products} = await server.get('/api/v1/products/1')
				.expect(200);

			expect(products.id_product).to.equal(1);
		});

		it('L\'id donné n\'existe pas alors j\'ai un 404', async () => {
			await server.get('/api/v1/products/888')
				.expect(404);
		});
	});

});