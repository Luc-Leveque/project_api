'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/uservente');

const server = request(createServer());

describe.only('Uservente api', function() {

    describe('GET /api/v1/uservente', function() {
        
        it('Recover all users', async () => {
			const {body: UserVente} = await server
				.get('/api/v1/uservente')
				.set('Accept', 'application/json')
				.expect(200);
			expect(UserVente).to.be.an('array');
        });
        
        it('Filtering uservente', async () => {
			const {body: UserVente} = await server
				.get('/api/v1/uservente')
				.query({
                    id_users: 1,
                    fullname: 'John Doe',
                    adress: '21 Malibu point',
                    mail: 'john@gmail.com',
                    tel: 33123456722

				})
				.set('Accept', 'application/json')
				.expect(200);

			expect(UserVente).to.be.an('array');
	
		});

    });

	describe('POST /api/v1/uservente', function () {
			before(async function() {
			await database.sequelize.query('DELETE from USERVENTES');
			const {UserVente} = database;
			await UserVente.create(fixtures);
		});

		it("La requete envoie toutes les données d'un utilisateur, utilisateur est crée et on reçoit un 200", async () => {
			await server.post('/api/v1/uservente')
				.send({
					id_users: 2,
                    fullname: 'Juliette Doe',
                    adress: '21 Malibu point',
                    mail: 'juliette@gmail.com',
                    tel: 33123456789
				})
				.expect(201);
		});

		it("La requete envoie un fullname qui existe déjà, on reçoit un 409", async () => {
			await server.post('/api/v1/uservente')
				.send({
					id_users: 1,
                    fullname: 'John Doe',
                    adress: '21 Malibu point',
                    mail: 'john@gmail.com',
                    tel: 33123456789
				})
				.expect(409);
		});

		it('should return an 400 error if given body has not all the mandatory data', async () => {
			const {body} = await server
				.post('/api/v1/uservente')
				.send({
					id_users: 2,
                    name: 'John Doe',//name != fullname
                    adress: '21 Malibu point',
                    mail: 'john@gmail.com',
                    tel: 33123456789
				})
				.expect(400);
		});
    });

    describe('PUT /api/v1/uservente/:id_users', function() {

        it("utilisateur existe renvoie un code 200", async () => {
			await server.put('/api/v1/uservente/1')
				.send({
					id_users: 1,
                    fullname: 'John Doe',
					adress: '21 Malibu point',
                    mail: 'john@gmail.com',
                    tel: 33123456789
				})
				.expect(200);
		});

		it("Il manque les données renvoie un code 400", async () => {
			await server.put('/api/v1/uservente/1')
				.send({
                    mail: 'john@gmail.com',
                    tel: 33123456789
				})
				.expect(400);
		});

		it("L'utilisateur n'existe pas donc une erreur 404", async() => {
            await server.put('/api/v1/uservente/idnonexistant')
                .expect(404);
        })
    })
});

describe.only('GET /api/v1/uservente/:id_users', function() {
    it("La reference donnée n'existe pas alors j'ai un 404", async () => {
        await server.get('/api/v1/uservente/je-n-existe-pas')
            .expect(404);
    });

    it("La reference donnée existe donc j'ai un 200 avec un compte client correspondant", async () => {
        const {body: uservente} = await server.get('/api/v1/uservente/1')
            .expect(200);

        expect(uservente.id_users).to.equal(1);
        expect(uservente.fullname).to.equal('John Doe');
    });

});

