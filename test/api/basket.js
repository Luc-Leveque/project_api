'use strict';

const {expect} = require('chai');
const request = require('supertest');
const createServer = require('../../app');
const database = require('../../src/database');
const fixtures = require('../fixtures/basket');

const server = request(createServer());

describe('Basket api', function() {

    before(async function() {
        await database.sequelize.query('DELETE from BASKETS');
        const {Basket} = database;
        const promises = fixtures.map(basket => Basket.create(basket));
		await Promise.all(promises);
    });



   
    describe.only('GET /api/v1/basket/:id_users', function() {

        it("L' id  donnée n'existe pas alors j'ai un 404", async () => {
            await server.get('/api/v1/basket/8')
                .expect(404);
        });
    
        it("La reference donnée existe donc j'ai un 200 avec un compte client correspondant", async () => {
            await server.get('/api/v1/basket/6').expect(200);

            const {body: basket} = await server
				.get('/api/v1/basket')
				.query({
                    "id": 2,
                    "id_users": 6,
                    "id_product": 2
				})
				.set('Accept', 'application/json')
				.expect(200);

  
                expect(basket).to.be.an('array');
                
		});

    });
    
    describe.only('DELETE /api/v1/basket/:id_users', function() {
  
        it("La reference donnée existe donc j'ai un 200 avec un compte client correspondant supprimé", async () => {
            await server.get('/api/v1/basket/6').expect(200);

            const {body: basket} = await server
				.delete('/api/v1/basket/6')
				.set('Accept', 'application/json')
				.expect(204);

		});

	});
	
	describe.only('POST /api/v1/basket', function () {

        it("La requete envoie toutes les données d'un utilisateur, utilisateur est crée et on reçoit un 200", async () => {
            await server.post('/api/v1/basket')
                .send({
                    id_users: 5,
                    id_product: 2
                })
                .expect(201);
        });

        it("La requête renvoie une bad request", async () => {
            await server.post('/api/v1/basket')
                .send({
                    id_usgtfhers: 5,
                    id_product: 2
                })
                .expect(400);
        });

    });

});