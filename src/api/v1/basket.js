const express = require('express');
const {BadRequest} = require('http-errors');
const router = express.Router();

router.get('/', async (req, res) => {

	const {Basket} = req.db;
	const basket = await Basket.findAll();

	res.send(basket);
});

router.get('/:id_users', async (req, res) => {
	const id_user = req.params.id_users
	const {Basket} = req.db;
	const basket = await Basket.findOne({ where: {id_users: id_user} });
	const basketList = await Basket.findAll({ where: {id_users: id_user} });
	if (basket) {
		return res.send(basketList);
	} else {
		return res.status(404)
			.send({message: `id_users ${id_user} not found`});
	}
});


router.post('/', async (req, res, next) => {
	try {
		const {body: givenBasket} = req;
		const {Basket} = req.db;
		const basket = await Basket.create(givenBasket);
		res.status(201).send(basket);
	} catch (err) {
			return res.status(400).send({message: 'Bad request'})
	}
});

router.delete('/:id_users', async (req, res) => {
    
    const id_user = req.params.id_users;
    const {Basket} = req.db;

	Basket.destroy({ where: {id_users: id_user} });

	return res.status(204).send({message: 'Panier vider'})


 });

module.exports = router;