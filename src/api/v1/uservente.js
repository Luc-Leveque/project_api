const express = require('express');
const {BadRequest} = require('http-errors');
const router = express.Router();



router.get('/', async (req, res) => {
	const {UserVente} = req.db;
	const user = await UserVente.findAll();

	res.send(user);
});

router.post('/', async(req, res) => {
	try {
		// Vérifier que il y a un username, fullname, country
		const body = req.body;
		if (body.fullname && body.adress && body.mail && body.tel) {
			// Insert dans la bdd
			const {UserVente} = req.db;
			const user = await UserVente.create(body);
			return res.status(201).send(user);
		}
		else {
			return res.status(400).send({message: 'Missing data'});
		}	
	} catch (err) {
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({message: 'User exists déjà'})
		}
	}
});


router.get('/:id_users', async (req, res) => {
	const id_users = req.params.id_users;
	const {UserVente} = req.db;
	const user = await UserVente.findOne({ where: {id_users: id_users} });
	if (user) {
		return res.send(user);
	} else {
		return res.status(404)
			.send({message: `Id_user: ${id_users} not found`});
	}
});

router.put('/:id_users', async (req, res) => {
    
    const id_users = req.params.id_users;
    const {UserVente} = req.db;
    const user = await UserVente.findOne({ where: {id_users: id_users} });

    if (user) {
        const body = req.body;
		if (body.fullname && body.adress && body.mail && body.tel) {
            user.update(body);
			return res.status(200).send(user);
		}
		else {
			return res.status(400).send({message: 'Missing data'});
		}	

    } else {
        return res.status(404)
            .send({message: `Id_user ${id_users} not found`});
    }
 });


module.exports = router;