const express = require('express');
const {BadRequest} = require('http-errors');
const router = express.Router();

router.get('/', async (req, res) => {

	const {Products} = req.db;
	const products = await Products.findAll();

	res.send(products);
});

router.get('/:id_product', async (req, res) => {
	const id_product = req.params.id_product;
	const {Products} = req.db;
	const product = await Products.findOne({ where: {id_product: id_product} });
	if (product) {
		return res.send(product);
	} else {
		return res.status(404)
			.send({message: `id_product ${id_product} not found`});
	}
});


router.post('/', async (req, res, next) => {
	try {
		const body = req.body;
		// Insert dans la bdd
		const {Products} = req.db;
		const product = await Products.create(body);
		res.status(201).send(product);
		
	} catch (err) {
		if (err.name === 'SequelizeValidationError') {
			return res.status(400).send({message: 'Bad request'})
		}
		if (err.name === 'SequelizeUniqueConstraintError') {
			return res.status(409).send({message: 'product exists déjà'})
		}	
	}
});

module.exports = router;