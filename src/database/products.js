'use strict';

const Products = (sequelize, DataTypes) => {
	return sequelize.define('Products', {
		id_product: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing id_product'}},
			allowNull: false,
			primaryKey: true		
		},
		name: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing name'}},
			allowNull: false
		},
		price: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing price'}},
			allowNull: false
		}
	});
};

module.exports = Products;
