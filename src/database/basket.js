'use strict';

const Basket = (sequelize, DataTypes) => {
	return sequelize.define('Basket', {
		id_users: {
			type: DataTypes.INTEGER,
            validate: {notEmpty: {msg: '-> Missing id_users'}},
            allowNull: false
		},
		id_product: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing id_product'}},
			allowNull: false
		}
	});
};

module.exports = Basket;
