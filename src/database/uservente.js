'use strict';

const UserVente = (sequelize, DataTypes) => {
	return sequelize.define('UserVente', {
		id_users: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true 
		},
		fullname: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing fullname'}},
			allowNull: false
		},
		adress: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing adress'}},
			allowNull: false
		},
		mail: {
			type: DataTypes.STRING,
			validate: {notEmpty: {msg: '-> Missing mail'}},
			allowNull: false
		},
		tel: {
			type: DataTypes.INTEGER,
			validate: {notEmpty: {msg: '-> Missing tel'}},
			allowNull: false
		},
	});
};

module.exports = UserVente;
